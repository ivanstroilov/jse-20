package ru.t1.stroilov.tm.api.service;

public interface IPropertyService extends ISaltProvider {

    String getApplicationVersion();

    String getAuthorEmail();

    String getAuthorName();

}
