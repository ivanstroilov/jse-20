package ru.t1.stroilov.tm;

import ru.t1.stroilov.tm.component.Bootstrap;

public class Application {

    public static void main(String... args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}