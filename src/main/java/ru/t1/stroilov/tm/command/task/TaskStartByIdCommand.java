package ru.t1.stroilov.tm.command.task;

import ru.t1.stroilov.tm.enumerated.Status;
import ru.t1.stroilov.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Start Task by ID.";

    public final static String NAME = "task-start-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(getUserId(), id, Status.IN_PROGRESS);
    }
}
